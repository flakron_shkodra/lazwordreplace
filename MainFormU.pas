unit MainFormU;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  EditBtn, ExtCtrls, Menus, ComCtrls, Buttons, ActnList, fgl, WordReplaceInfo,
  strutils;

type

  { TMainForm }

  TMainForm = class(TForm)
    actAddWord: TAction;
    actExit: TAction;
    actAbout: TAction;
    actClearWordList: TAction;
    actRemoveWord: TAction;
    actNewWordList: TAction;
    actStartReplaceProcess: TAction;
    actLoadWordList: TAction;
    actSaveWordListAs: TAction;
    actLoadFiles: TAction;
    actSaveWordList: TAction;
    AppActionList: TActionList;
    btnAdd: TBitBtn;
    btnAbout: TBitBtn;
    btnSave: TToolButton;
    btnStartReplacment: TBitBtn;
    btnClose: TBitBtn;
    grpTextFiles: TGroupBox;
    AppImages: TImageList;
    lblPlainTextFiles: TLabel;
    lstFiles: TListBox;
    lstShortFilenames: TListBox;
    MenuItem1: TMenuItem;
    mitFile: TMenuItem;
    mitExit: TMenuItem;
    mitWordList: TMenuItem;
    mitOpenWordList: TMenuItem;
    mitSavWordList: TMenuItem;
    mitSaveAsWordList: TMenuItem;
    mitHelp: TMenuItem;
    mitAbout: TMenuItem;
    mitOpen: TMenuItem;
    MenuItem9: TMenuItem;
    dlgOpenWordList: TOpenDialog;
    dlgSaveWordList: TSaveDialog;
    tbtnLoadfFiles: TToolButton;
    tbtnLoadWordList: TToolButton;
    tbtnSaveAss: TToolButton;
    tlbWordList: TToolBar;
    tlbFiles: TToolBar;
    ToolButton1: TToolButton;
    tbtnClearWordList: TToolButton;
    txtSearch: TEdit;
    grpWords: TGroupBox;
    lblReplaceWith: TLabel;
    lblFindWord: TLabel;
    lblCurrentFile: TLabel;
    lblTotal: TLabel;
    lstWords: TListBox;
    AppMenu: TMainMenu;
    mitRemoveWord: TMenuItem;
    dlgOpenFiles: TOpenDialog;
    pnlMain: TPanel;
    pbCurrent: TProgressBar;
    pmRemove: TPopupMenu;
    pbTotal: TProgressBar;
    txtBaseWord: TEdit;
    txtReplacementWord: TEdit;
    procedure actAboutExecute(Sender: TObject);
    procedure actAddWordExecute(Sender: TObject);
    procedure actClearWordListExecute(Sender: TObject);
    procedure actExitExecute(Sender: TObject);
    procedure actLoadFilesExecute(Sender: TObject);
    procedure actLoadWordListExecute(Sender: TObject);
    procedure actRemoveWordExecute(Sender: TObject);
    procedure actSaveWordListAsExecute(Sender: TObject);
    procedure actSaveWordListExecute(Sender: TObject);
    procedure actStartReplaceProcessExecute(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure btnLoadFilesClick(Sender: TObject);
    procedure btnSaveListClick(Sender: TObject);
    procedure btnStartReplacementClick(Sender: TObject);
    procedure FileNameEdit1AcceptFileName(Sender: TObject; var Value: string);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormDropFiles(Sender: TObject; const FileNames: array of String);
    procedure lstShortFilenamesClick(Sender: TObject);
    procedure lstWordsClick(Sender: TObject);
    procedure lstWordsSelectionChange(Sender: TObject; User: boolean);
    procedure mitRemoveWordClick(Sender: TObject);
    procedure txtSearchChange(Sender: TObject);
    procedure txtSearchEnter(Sender: TObject);
    procedure txtSearchExit(Sender: TObject);
  private
    procedure StartReplacementProcess;
    procedure LoadWordListFromInfoList;
    { private declarations }
  public
    { public declarations }
  end;



var
  MainForm: TMainForm;
  wrInfoList: TWordReplaceInfoList;
  wrModified: boolean;
  wrFilename: string;
  breakProcession: boolean;
  lstDelimited: TStringList;

implementation


{$R *.lfm}

{ TMainForm }

procedure TMainForm.FileNameEdit1AcceptFileName(Sender: TObject; var Value: string);
begin
  if FileIsText(Value) then
  begin
    lstFiles.Items.Add(Value);
  end;
end;

procedure TMainForm.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  if wrModified then
    if MessageDlg('Modified',
      'Word List has been modified. Would you like to save it?',
      mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      wrInfoList.SaveToFile(wrFilename);

end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  Caption := Application.Title;
  lstDelimited := TStringList.Create;
  lstDelimited.Delimiter := '=';
  lstDelimited.StrictDelimiter := True;

  wrFilename := ChangeFileExt(Application.ExeName, '.words');
  wrInfoList := TWordReplaceInfoList.Create(True);
  if FileExists(wrFilename) then
  begin
    wrInfoList.LoadFromFile(wrFilename);
    LoadWordListFromInfoList;
  end
  else
  begin
    ShowMessage('No words have been set yet');
  end;
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  wrInfoList.Free;
  lstDelimited.Free;
end;

procedure TMainForm.FormDropFiles(Sender: TObject;
  const FileNames: array of String);
var
  I: Integer;
begin
  for I:=0 to Length(Filenames)-1 do
  begin
    lstFiles.Clear;
    lstShortFilenames.Clear;
    if FileIsText(FileNames[I]) and
      (LowerCase(ExtractFileExt(FileNames[I])) <> '.words') then
    begin
      lstFiles.Items.Add(FileNames[I]);
      lstShortFilenames.Items.Add(ExtractFileName(FileNames[I]));
    end;
  end;
end;

procedure TMainForm.lstShortFilenamesClick(Sender: TObject);
begin

end;

procedure TMainForm.lstWordsClick(Sender: TObject);
begin
  if (lstWords.ItemIndex > -1) and (lstWords.ItemIndex < lstWords.Count) then
  begin
    lstDelimited.DelimitedText := lstWords.Items[lstWords.ItemIndex];
    txtBaseWord.Text := lstDelimited[0];
    txtReplacementWord.Text := lstDelimited[1];
  end;
end;

procedure TMainForm.lstWordsSelectionChange(Sender: TObject; User: boolean);
begin
  try
    txtBaseWord.Text := lstWords.Items[lstWords.ItemIndex];
  except
  end;
end;

procedure TMainForm.mitRemoveWordClick(Sender: TObject);
begin

end;

function FindListItem(const List: TStrings; SearchValue: string): integer;
begin
  for Result := 0 to List.Count - 1 do
  begin
    if AnsiContainsText(List[Result], SearchValue) then
      Exit;
  end;
  Result := -1;
end;

procedure TMainForm.txtSearchChange(Sender: TObject);
var
  i: integer;
begin
  i := FindListItem(lstWords.Items, txtSearch.Text);
  if (i > -1) and (i < lstWords.Count) then
    lstWords.ItemIndex := i;
end;

procedure TMainForm.txtSearchEnter(Sender: TObject);
begin
  txtSearch.Clear;
end;

procedure TMainForm.txtSearchExit(Sender: TObject);
begin
  txtSearch.Text := 'Quick Search';
end;

procedure TMainForm.StartReplacementProcess;
begin

end;

procedure TMainForm.LoadWordListFromInfoList;
var
  I: integer;
begin
  lstWords.Clear;
  for I := 0 to wrInfoList.Count - 1 do
  begin
    lstWords.Items.Add(wrInfoList[I].BaseWord + '=' + wrInfoList[I].ReplacementWord);
  end;
end;

procedure TMainForm.btnLoadFilesClick(Sender: TObject);
begin

end;

procedure TMainForm.btnSaveListClick(Sender: TObject);
begin

end;

procedure TMainForm.btnStartReplacementClick(Sender: TObject);
begin
  StartReplacementProcess;

end;
procedure TMainForm.actLoadFilesExecute(Sender: TObject);
var
  I: integer;
begin
  if dlgOpenFiles.Execute then
  begin
    lstFiles.Clear;
    lstShortFilenames.Clear;
    for I := 0 to dlgOpenFiles.Files.Count - 1 do
    begin
      if FileIsText(dlgOpenFiles.Files[I]) and
        (LowerCase(ExtractFileExt(dlgOpenFiles.Files[I])) <> '.words') then
      begin
        lstFiles.Items.Add(dlgOpenFiles.Files[I]);
        lstShortFilenames.Items.Add(ExtractFileName(dlgOpenFiles.Files[I]));
      end;
    end;
  end;
end;

procedure TMainForm.actLoadWordListExecute(Sender: TObject);
begin
  if dlgOpenWordList.Execute then
  begin
    wrInfoList.LoadFromFile(dlgOpenWordList.FileName);
    LoadWordListFromInfoList;
  end;
end;

procedure TMainForm.actRemoveWordExecute(Sender: TObject);
begin
  if lstWords.ItemIndex > -1 then
  begin
    wrInfoList.Delete(lstWords.ItemIndex);
    LoadWordListFromInfoList;
    wrModified := True;
  end;
end;

procedure TMainForm.actSaveWordListAsExecute(Sender: TObject);
begin

  if wrInfoList.Count < 1 then
  begin
    ShowMessage('Word replacment list is empty. Nothing to save');
    Exit;
  end;

  if dlgSaveWordList.Execute then
  begin
    if FileExists(dlgSaveWordList.FileName) then
      if MessageDlg('Overwrite',
        'A file with the same name has been found. Would you like to replace it?',
        mtConfirmation, mbYesNo, 0) = mrNo then
        Exit;

    lstWords.Items.SaveToFile(dlgSaveWordList.FileName);
  end;
end;

procedure TMainForm.actSaveWordListExecute(Sender: TObject);
begin
  wrInfoList.SaveToFile(wrFilename);
  wrInfoList.LoadFromFile(wrFilename);
end;

procedure TMainForm.actStartReplaceProcessExecute(Sender: TObject);
var
  lstLines: TStringList;
  J: integer;
  I: integer;
begin
  if lstFiles.Count < 1 then
  begin
    ShowMessage('No plain text files loaded');
    Exit;
  end;

  if lstWords.Count < 1 then
  begin
    ShowMessage('No words for replacement.Add some words to replace then, try again.');
    Exit;
  end;

  if MessageDlg('Warning', 'It is recommended to backup your files before you proceed. Are you sure you want to continue?',
    mtWarning, mbYesNo, 0) = mrNo then
    Exit;


  if btnStartReplacment.Caption = 'Start' then
  begin
    breakProcession := False;
    btnStartReplacment.Caption := 'Cancel';
    pbTotal.Max := lstFiles.Count;
    pbTotal.Step := 1;
    pbTotal.Position := 0;

    pbCurrent.Max := lstWords.Count;
    pbCurrent.Step := 1;

    lstLines := TStringList.Create;
    try
      for I := 0 to lstFiles.Count - 1 do
      begin
        lstLines.LoadFromFile(lstFiles.Items[I]);
        pbCurrent.Position := 0;
        lblCurrentFile.Caption := 'Current File: ' + lstFiles.Items[I];
        Application.ProcessMessages;
        for J := 0 to wrInfoList.Count - 1 do
        begin
          lstLines.Text := AnsiReplaceStr(lstLines.Text,
            wrInfoList[J].BaseWord, wrInfoList[J].ReplacementWord);
          pbCurrent.StepIt;
        end;
        lstLines.SaveToFile(lstFiles.Items[I]);
        pbTotal.StepIt;
        Application.ProcessMessages;
        lblTotal.Caption := 'Total Progress ' + IntToStr(I) + '/' + IntToStr(
          lstFiles.Count);
        if breakProcession then
          Break;
      end;
      Application.MessageBox('Word replacement complete', 'Info', 0);
      pbCurrent.Position := 0;
      pbTotal.Position := 0;
      lblCurrentFile.Caption := 'Current File Progress';
      lblTotal.Caption := 'Total Progress';

    finally
      lstLines.Free;
      btnStartReplacment.Caption := 'Start';
    end;
  end
  else
  begin
    breakProcession := True;
  end;
end;

procedure TMainForm.actAddWordExecute(Sender: TObject);
var
  wInfo: TWordReplaceInfo;
  SelectedIndex: integer;
begin
  if Trim(txtBaseWord.Text) = EmptyStr then
  begin
    ShowMessage('Find Word is Empty');
    Exit;
  end;
  if Trim(txtReplacementWord.Text) = EmptyStr then
  begin
    ShowMessage('Replace with word is empty');
    Exit;
  end;

  wInfo := wrInfoList.Find(txtBaseWord.Text);

  if wInfo = nil then
  begin
    wInfo := TWordReplaceInfo.Create;
    wInfo.BaseWord := txtBaseWord.Text;
    wInfo.ReplacementWord := txtReplacementWord.Text;
    wrInfoList.Add(wInfo);
    if lstWords.Count = 0 then
      SelectedIndex := 0
    else
      SelectedIndex := lstWords.Count - 1;
  end
  else
  begin
    wInfo.ReplacementWord := txtReplacementWord.Text;
    SelectedIndex := lstWords.ItemIndex;
  end;

  LoadWordListFromInfoList;
  wrModified := True;
  txtBaseWord.Clear;
  txtReplacementWord.Clear;
  txtBaseWord.SetFocus;
  lstWords.ItemIndex := SelectedIndex;
  lstWordsClick(Sender);
end;

procedure TMainForm.actClearWordListExecute(Sender: TObject);
begin
  lstWords.Clear;
  wrInfoList.Clear;
  wrModified := True;
end;

procedure TMainForm.actAboutExecute(Sender: TObject);
begin
  ShowMessage('WordReplacerLZ 0.1' + LineEnding +
    '================================================' + LineEnding +
    ' This is a program to replace words in all text files ' + LineEnding +
    'if match found in word list.' + LineEnding +
    'Text files may be of any extension (*.txt;*.cs;*.pas... and all other' + LineEnding +
    'readable plain text files)' + LineEnding +
    '================================================' + LineEnding +
    'Flakron Shkodra 2011');
end;

procedure TMainForm.actExitExecute(Sender: TObject);
begin
  Close;
end;


procedure TMainForm.btnCloseClick(Sender: TObject);
begin
  Application.Terminate;
end;

end.

