unit WordReplaceInfo;

{$mode objfpc}{$H+}

interface
uses
  Classes, SysUtils,FGL;

type
{ TWordReplaceInfo }

  TWordReplaceInfo = class
  private
    FBaseWord: string;
    FReplacementWord: string;
    procedure SetBaseWord(const AValue: string);
    procedure SetReplacementWord(const AValue: string);
  public
    property BaseWord:string read FBaseWord write SetBaseWord;
    property ReplacementWord:string read FReplacementWord write SetReplacementWord;
  end;

  { TWordReplaceInfoList }

  TWordReplaceInfoList = class(specialize TFPGObjectList<TWordReplaceInfo>)
  public
    procedure LoadFromFile(Filename:string);
    procedure SaveToFile(Filename:string);
    function Exists(BaseWord:string):Boolean;
    function Find(BaseWord:string):TWordReplaceInfo;
  end;



implementation

{ TWordReplaceInfoList }

procedure TWordReplaceInfoList.LoadFromFile(Filename: string);
var
  lstWords:TStringList;
  lstDel:TStringList;
  I: Integer;
  J: Integer;
  wrInfo:TWordReplaceInfo;
begin

  Self.Clear;

  try
     lstWords := TStringList.Create;
     lstWords.LoadFromFile(Filename);

     lstDel := TStringList.Create;
     lstDel.Delimiter:='=';
     lstDel.StrictDelimiter:=True;

     for I:=0 to lstWords.Count-1 do
     begin
       lstDel.DelimitedText:=lstWords[I];
       if lstDel.Count=2 then
       begin
         wrInfo := TWordReplaceInfo.Create;
         wrInfo.BaseWord:=lstDel[0];
         wrInfo.ReplacementWord:=lstDel[1];
         Add(wrInfo);
       end;
     end;

  finally
    lstWords.Free;
    lstDel.Free;
  end;
end;

procedure TWordReplaceInfoList.SaveToFile(Filename: string);
var
  I: Integer;
  lst:TStringList;
begin
  try
    lst := TStringList.Create;
    for I:=0 to Count - 1 do
    begin
      lst.Add(Items[I].BaseWord+'='+Items[I].ReplacementWord);
    end;
    lst.SaveToFile(Filename);
  finally
    lst.Free;
  end;
end;

function TWordReplaceInfoList.Exists(BaseWord: string): Boolean;
var
  I: Integer;
begin
  Result:=False;
  for I:=0 to Count-1 do
  begin
    if Items[I].BaseWord = BaseWord then
    begin
      Result:=True;
      Break;
    end;
  end;
end;

function TWordReplaceInfoList.Find(BaseWord: string): TWordReplaceInfo;
var
  I: Integer;
begin
  Result:=nil;
  for I:=0 to Count-1 do
  begin
    if Items[I].BaseWord = BaseWord then
    begin
      Result:=Items[I];
      Break;
    end;
  end;
end;


{ TWordReplaceInfo }

procedure TWordReplaceInfo.SetBaseWord(const AValue: string);
begin
  if FBaseWord=AValue then exit;
  FBaseWord:=AValue;
end;

procedure TWordReplaceInfo.SetReplacementWord(const AValue: string);
begin
  if FReplacementWord=AValue then exit;
  FReplacementWord:=AValue;
end;

end.

